<?php

namespace Modules\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Customer\Http\Requests\CustomerRequest;
use Modules\Customer\Repositories\CustomerRepositoryInterface;

class CustomerController extends Controller
{
    protected $customerRepo;

    public function __construct(CustomerRepositoryInterface $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $pageSize = $request->page_size ?? PAGE_SIZE_DEFAULT;
        $customers = $this->customerRepo->getItemsBuilder($request)->paginate($pageSize);
        return response()->json($customers);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(CustomerRequest $request)
    {
        return view('customer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CustomerRequest $request)
    {
        $data = $request->only(['name', 'phone', 'email', 'note', 'address']);
        return $this->customerRepo->create($data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $customer = $this->customerRepo->detail($id);
        return $this->sendSuccess($customer);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('customer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(CustomerRequest $request, $id)
    {
        $customer = $request->all();
        return $this->customerRepo->update($customer, $id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $status =  $this->customerRepo->delete($id);
        return $this->sendSuccess($status);
    }
}
