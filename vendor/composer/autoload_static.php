<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit07bc553469dde0442fabea2caa6d4da7
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\Customer\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\Customer\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit07bc553469dde0442fabea2caa6d4da7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit07bc553469dde0442fabea2caa6d4da7::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
