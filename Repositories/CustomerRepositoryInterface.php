<?php

namespace Modules\Customer\Repositories;

use App\Contracts\Repositories\BaseRepositoryInterface;
use Illuminate\Http\Request;

interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * getItems
     *
     * @param  Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getItemsBuilder(Request $request);
}
